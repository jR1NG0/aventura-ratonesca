﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemigoCatInm : MonoBehaviour {

	//Variables gestionar radio vision,ataque y velocidad mv
	public float visionRadius;
	public float attackRadius;
	public float speed;

	//variable para guardar jugador
	GameObject player;

	//variable posicion incial
	Vector3 initialPosition;

	//animador y cuerpo cinematico con la rotacion en z congelada
	Animator anim;
	Rigidbody2D rb2d;


	// Use this for initialization
	void Start () {
		 //recuperamos al jugador gracias al tag 
		 player = GameObject.FindGameObjectWithTag("Player");

		 //guardamos nuestra posicion inicial
		 initialPosition = transform.position;

		 anim = GetComponent<Animator>();
		 rb2d = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
		//por defecto nuestro target siempre sera nuestra posicion inicial
		Vector3 target = initialPosition;

		//comprobamos un raycast del enemigo hasta el jugador
		RaycastHit2D hit = Physics2D.Raycast(
			transform.position,
			player.transform.position - transform.position,
			visionRadius,
			1 << LayerMask.NameToLayer("Default")
			//poner el propeio enemigo en un layer distinta a default para evitar el raycast
			//tambien al objeto atack y al prefab slash una layer atack
			//sino los detectara como entorno y se mueve atras al hacer ataques
		);

		//aqui podemos debugear e raycast
		Vector3 forward = transform.TransformDirection(player.transform.position - transform.position);
		Debug.DrawRay(transform.position,forward,Color.red);

		//si el raycast encuentra al jugador lo ponemos de target
		if(hit.collider != null){
			if(hit.collider.tag == "player"){
				target = player.transform.position;
			}
		}

		//calculamos la distancia y la direccion actual hasta el target

		float distance = Vector3.Distance(target,transform.position);
		Vector3 dir = (target - transform.position).normalized;

		//si es el enemigo y está en rango de ataque nos paramos y le atacamos
		if(target != initialPosition && distance < attackRadius){
			//aqui le atacariamos, pero por ahora simplemente cambiamos la animacion
			//
			//
			//
			SceneManager.LoadScene(1);
		}
		//en caso contrario nos movemos hacia el
		else{
			rb2d.MovePosition(transform.position + dir*speed*Time.deltaTime);

			anim.speed = 1;
			anim.SetBool("caminar", true);

		}

		//una ultima comprobacion para evitar bugs forzando la posicion incial 
		//if(target == initialPosition && distance < 0.02f){
		//	transform
		//}

	}

	void OnDrawGizmosSelected(){
			Gizmos.color = Color.yellow;
			Gizmos.DrawWireSphere(transform.position,visionRadius);
			Gizmos.DrawWireSphere(transform.position,attackRadius);
	}
}
