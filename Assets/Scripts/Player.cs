﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.D)){
			transform.Translate(0.016f,0,0);
			GetComponent<Animator>().SetBool("correrDerecha",true);
			
		}
		if(Input.GetKey(KeyCode.A)){
			transform.Translate(-0.016f,0,0);
			GetComponent<Animator>().SetBool("correrIzquierda",true);
		}
		if(Input.GetKey(KeyCode.W)){
			transform.Translate(0,0.016f,0);
			GetComponent<Animator>().SetBool("correrArriba",true);
		}
		if(Input.GetKey(KeyCode.S)){
			transform.Translate(0,-0.016f,0);
			GetComponent<Animator>().SetBool("correrAbajo",true);
		}
		if(Input.GetKeyUp(KeyCode.D)||Input.GetKeyUp(KeyCode.A)||Input.GetKeyUp(KeyCode.W)||Input.GetKeyUp(KeyCode.S)){
			GetComponent<Animator>().SetBool("correrDerecha",false);
			GetComponent<Animator>().SetBool("correrIzquierda",false);
			GetComponent<Animator>().SetBool("correrArriba",false);
			GetComponent<Animator>().SetBool("correrAbajo",false);
		}
	}
}
